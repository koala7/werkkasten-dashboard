# werkkasten-dashboard

The werkkasten-dashboard is a simple GUI which provides easy access to several things which are cumbersome in nature.

For example to trigger the release pipelines to release either a _patch_, _minor_ or _major_ version of the werkkasten.


Idea behind this app is mostly to get comfortable with Vue.js 3 before migrating the werkkasten front-end to the new version.


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
